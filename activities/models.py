from django.contrib.postgres.fields import JSONField
from django.db import models


class Property(models.Model):
    title = models.CharField(max_length=255, null=False)
    address = models.TextField(null=False)
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=False)
    updated_at = models.DateTimeField(auto_now=True, null=False)
    disabled_at = models.DateTimeField(null=True)
    STATUS_CHOICE = (
        ("a", "active"),
        ("n", "noactive"),
    )
    status = models.CharField(
        max_length=35,
        null=False,
        choices=STATUS_CHOICE,
    )

    def __str__(self):
        return self.title[:25]


class Activity(models.Model):
    property_id = models.ForeignKey(
        Property,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name="activities",
    )
    schedule = models.DateTimeField(null=False)
    title = models.CharField(max_length=255, null=False)
    created_at = models.DateTimeField(auto_now_add=True, null=False)
    updated_at = models.DateTimeField(auto_now=True, null=False)
    STATUS_CHOICE = (
        ("a", "active"),
        ("d", "done"),
        ("c", "cancelled"),
    )
    status = models.CharField(
        max_length=35,
        null=False,
        choices=STATUS_CHOICE,
    )

    def __str__(self):
        return self.title[:25]


class Survey(models.Model):
    activity_id = models.OneToOneField(
        Activity,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name="surveys",
    )
    answers = JSONField(null=False)
    created_at = models.DateTimeField(auto_now_add=True, null=False)
