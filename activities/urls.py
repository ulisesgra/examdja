from django.conf.urls import url

from .views import (
    ActivityAllList,
    ActivityCancel,
    ActivityCreate,
    ActivityDelete,
    ActivityList,
    ActivityReschedule,
)

urlpatterns = [
    # url(r"^activity/(?P<pk>[0-9]+)/$", ActivityDetail.as_view()),
    #
    url(r"^delete/activity/(?P<pk>[0-9]+)/$", ActivityDelete.as_view()),
    url(r"^create/activity/", ActivityCreate.as_view()),
    url(
        r"^reschedule/activity/(?P<pk>[0-9]+)/$",
        ActivityReschedule.as_view(),
    ),
    url(r"^cancel/activity/(?P<pk>[0-9]+)/$", ActivityCancel.as_view()),
    url(r"^activities/", ActivityList.as_view()),
    url(r"^all/activities/", ActivityAllList.as_view()),
    #
    # url(r"^surveys/(?P<pk>[0-9]+)/$", SurveyDetail.as_view()),
]
