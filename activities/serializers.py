from django.utils import timezone
from rest_framework import serializers

from .models import Activity, Property, Survey


class PropertySerializer(serializers.ModelSerializer):
    class Meta:
        model = Property
        fields = (
            "id",
            "title",
            "address",
        )


class ActivitySerializer(serializers.ModelSerializer):
    condition = serializers.SerializerMethodField()
    property_id = PropertySerializer(read_only=True)
    # survey = serializers.HyperlinkedIdentityField(view_name="surveys-detail")

    class Meta:
        model = Activity
        fields = (
            "id",
            "schedule",
            "title",
            "created_at",
            "status",
            "condition",
            "property_id",
            # "survey",
        )

    def get_condition(self, obj):
        # date_now = datetime.datetime.now()
        date_now = timezone.now()
        if obj.status == "a" and obj.schedule >= date_now:
            return "Pendiente de realizar"
        if obj.status == "a" and obj.schedule < date_now:
            return "Atrasada"
        if obj.status == "d":
            return "Finalizada"


class ActivityCancelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = (
            "id",
            "status",
        )


class ActivityRescheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = (
            "id",
            "schedule",
        )


# =====================================================================
class PropertyCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Property
        fields = (
            "id",
            "title",
            "address",
            "description",
            "disabled_at",
            "status",
        )


class SurveyCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Survey
        fields = (
            "id",
            "activity_id",
            "answers",
        )


class ActivityCreateSerializer(serializers.ModelSerializer):
    # property_id = PropertyCreateSerializer(
    #     # many=True,
    #     # not compatible with django version
    #     # queryset=Property.objects.all(),
    # )

    class Meta:
        model = Activity
        fields = (
            "id",
            "property_id",
            "schedule",
            "title",
            "status",
        )
