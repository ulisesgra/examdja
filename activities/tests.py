import datetime

from django.test import TestCase

from .models import Activity, Property, Survey


# Create your tests here.
class ActivityModelsTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        # create property
        property1 = Property(
            title="pro1_title",
            address="pro1_address",
            description="pro1_description",
            status="status1",
        )
        property1.save()

        # create activity
        activity1 = Activity(
            property_id=property1,
            schedule=datetime.date(2010, 1, 1),
            title="act1_title",
            status="activate",
        )
        activity1.save()

        # create survey
        survey1 = Survey(
            activity_id=activity1,
            answers={"url": "url1"},
        )
        survey1.save()

    def test_activity_content(self):
        act = Activity.objects.get(id=1)
        act_title = f"{act.title}"
        act_status = f"{act.status}"
        self.assertEqual(act_title, "act1_title")
        self.assertEqual(act_status, "activate")
