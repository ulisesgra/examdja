# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2021-12-16 21:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('activities', '0003_auto_20211216_2141'),
    ]

    operations = [
        migrations.AlterField(
            model_name='property',
            name='status',
            field=models.CharField(choices=[('a', 'active'), ('n', 'noactive')], max_length=35),
        ),
    ]
