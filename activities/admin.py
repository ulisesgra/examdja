from django.contrib import admin

from .models import Activity, Property, Survey


# ============================
class ActivityInline(admin.TabularInline):
    model = Activity


class PropertyAdmin(admin.ModelAdmin):
    inlines = [
        ActivityInline,
    ]


admin.site.register(Activity)
admin.site.register(Property, PropertyAdmin)
admin.site.register(Survey)


# ============================
# admin.site.register(Activity)
# admin.site.register(Survey)
# admin.site.register(Property)
#
