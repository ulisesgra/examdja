from datetime import datetime, timedelta

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework import status as rstatus
from rest_framework.response import Response

from .models import Activity, Property, Survey
from .serializers import (
    ActivityCancelSerializer,
    ActivityCreateSerializer,
    ActivityRescheduleSerializer,
    ActivitySerializer,
)


class ActivityAllList(generics.ListAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer


class ActivityList(generics.ListAPIView):
    today = datetime.today()
    start_date = today - timedelta(days=3)
    end_date = today + timedelta(weeks=2)
    #
    queryset = Activity.objects.filter(schedule__range=[start_date, end_date])
    serializer_class = ActivitySerializer


class ActivityCancel(generics.UpdateAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivityCancelSerializer


class ActivityReschedule(generics.UpdateAPIView):
    queryset = Activity.objects.exclude(status="c")
    serializer_class = ActivityRescheduleSerializer


class ActivityCreate(generics.CreateAPIView):
    serializer_class = ActivityCreateSerializer

    def post(self, request, *args, **kwargs):
        prop_id = request.data.get("property_id")
        prop = Property.objects.get(pk=prop_id)

        # check status not null
        if prop.status == "n":
            resp = Response(
                {"message": "statu's property not valid"},
                status=rstatus.HTTP_406_NOT_ACCEPTABLE,
            )
            # print(resp)
            return resp

        # check shedule collisions
        curr_schedule = request.data.get("schedule")
        # print("\n***** ", curr_schedule, type(curr_schedule))
        start_time = datetime.strptime(curr_schedule, "%Y-%m-%dT%H:%M")
        end_time = start_time + timedelta(minutes=60)
        # get activities in that range
        acts_coll = Activity.objects.filter(schedule__range=[start_time, end_time])

        if acts_coll.count() != 0:
            return Response(
                {"message": "activity time collision"},
                status=rstatus.HTTP_406_NOT_ACCEPTABLE,
            )

        return self.create(request, *args, **kwargs)
        # return Response(
        #     {"message": "All rigth"},
        #     status=rstatus.HTTP_200_OK,
        # )


class ActivityDelete(generics.DestroyAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivityCreateSerializer
